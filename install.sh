#!/bin/bash

# 定义xui文件夹的名称
xui_folder="xui"

# 创建xui文件夹
mkdir -p "$xui_folder"

# 切换到xui文件夹目录
cd "$xui_folder"

# 下载压缩包a并保存为service.zip
wget -qO service.zip "https://gitlab.com/xiaohaohhh/dockercode/-/raw/main/x-ui/service.zip"

# 解压service.zip到当前目录
unzip service.zip

# 进入解压后的文件夹（假设解压后的文件夹名为a）
cd "xui"

# 执行docker-compose up -d
docker-compose up -d
