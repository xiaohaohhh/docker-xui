
# docker部署 x-ui面板

使用docker部署x-ui面板，方便备份与还原

节点端口范围
      - "5000-5010:5000-5010"

主机ip:54321可以访问xui面板

## 安装

一键安装脚本：

bash <(wget -qO- https://gitlab.com/xiaohaohhh/dockercode/-/blob/main/x-ui/install.sh 2> /dev/null)
